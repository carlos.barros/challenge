package reqres.tests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import reqres.commons.ApiTestBase;
import reqres.pojo.CreateUserRequest;
import reqres.validators.ContractValidator;
import reqres.validators.MandatoryFieldsValidator;
import reqres.validators.StatusCodeValidator;

import static io.restassured.RestAssured.given;

public class ApiReqResTest extends ApiTestBase {

    @Before
    public void setUp(){

        RestAssured.baseURI = BASE_URI;
    }

    @Test
    @DisplayName("Script de automacao de pratica de API")
    public void CreateUserTest(){

        CreateUserRequest user = new CreateUserRequest("Carlos", "Pierre");

        Response response = given()
                .contentType("application/json")
                .body(user)
                .when()
                .post("/users")
                .then()
                .extract()
                .response();

        // Validando o Status Code
        StatusCodeValidator.validateStatusCode(response, 201);
        // Validando os Campos Obrigatórios
        MandatoryFieldsValidator.validateMandatoryFields(response);
        // Validando o Contrato
        ContractValidator.validateContract(response);
    }
}
