package reqres.validators;

import io.restassured.response.Response;

public class StatusCodeValidator {

    public static void validateStatusCode(Response response, int expectedStatusCode) {
        response.then().statusCode(expectedStatusCode);
    }
}
