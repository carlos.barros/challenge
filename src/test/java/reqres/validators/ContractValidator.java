package reqres.validators;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ContractValidator {
    public static void validateContract(Response response) {
         JsonPath jsonPath = response.jsonPath();
         assert jsonPath.get("id") != null;
         assert jsonPath.get("firstName") != null;
         assert jsonPath.get("lastName") != null;
    }
}
