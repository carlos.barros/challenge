# Projeto de Automação de Testes com Java 17 e RestAssured

Este projeto de automação de testes é desenvolvido utilizando Java 17 e a biblioteca RestAssured para realizar testes de APIs REST. Abaixo, você encontrará informações sobre como executar um teste na linha de comando e como criar o projeto do zero ou fazer o clone de um repositório GitLab.

## Tecnologias Utilizadas

- **Java 17**: A linguagem de programação utilizada para escrever os testes de automação.

- **RestAssured**: Uma biblioteca Java amplamente utilizada para automatizar testes em serviços RESTful.

## Executando um Teste na Linha de Comando

Para executar um teste na linha de comando, siga os passos abaixo:

1. Clone o repositório GitLab ou crie o projeto do zero (consulte as seções a seguir).
2. Abra um terminal ou prompt de comando.
3. Navegue até o diretório do projeto.
4. Execute o seguinte comando para executar o teste:

   ```shell
   mvn test

## Criando o projeto do zero 

1. Certifique-se de que o Java 17 esteja instalado em seu sistema.
2. Configure um novo projeto Maven com base no Java 17. Você pode usar a seguinte estrutura de diretórios como referência:

/seu-projeto
├── src/main/java
│   └── ...
├── src/test/java
│   └── ...
├── pom.xml

3. Adicione as dependências necessárias ao seu arquivo pom.xml. Certifique-se de incluir a dependência para o RestAssured.
4. Escreva seus testes de automação em Java utilizando o RestAssured.
5. Siga as instruções da seção "Executando um Teste na Linha de Comando" para executar seus testes.

## Fazendo Clone de um Repositório GitLab

1. Certifique-se de que o Git esteja instalado em seu sistema.
2. No GitLab, encontre o repositório que deseja clonar.
3. Copie a URL do repositório Git (por exemplo, git@gitlab.com/seu-usuario/seu-projeto.git).
4. Abra um terminal ou prompt de comando.
5. Navegue até o diretório onde deseja clonar o repositório.
6. Execute o seguinte comando, substituindo URL_DO_REPOSITORIO pela URL que você copiou anteriormente:

git clone URL_DO_REPOSITORIO

- O repositório será clonado para o seu sistema. Após o clone, siga as instruções da seção "Executando um Teste na Linha de Comando" para executar os testes do projeto.